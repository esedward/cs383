//Code started from agentstarter

class Agent {

  // current position
  float x, y;
  // previous position
  float px, py;
  //lifespan
  int age;
  Boolean dead = false;
  
  //attributes
  color colorD; // D for Dominant
  color colorR; // R for Recessive
  float weight; // size
  String sex; // M/F
  Boolean curlyD; // curly wings determines noiseZ
  Boolean curlyR;
  
  //Noise code inspired by M1503
  float noiseScale;
  float noiseZ;
  float noiseZVelocity = 0.01;
  float angle;

  // create agent with random position and attributes
  Agent() {
    // random starting position
    int m = 0; // margin
    x = random(m, width - m);
    y = random(m, height - m);
    while(dist(x, y, width/2, height/2) > int(radius) + weight + 10) {
      x = random(m, width - m);
      y = random(m, height - m);
    }
    px = x;
    py = y;
    
    // attributes
    age = millis();
    setSex();
    if (sex == "M") { //male
      colorD = color(int(random(0, 360)), //hue
      //int(random(0, 100)), //saturation
      100,
      int(random(25, 50))); //brightness
      weight = random(maxWeight-weightThreshold, maxWeight); //size
    }
    else { //female
      colorD = color(int(random(0, 360)), //hue
      //int(random(0, 100)), //saturation
      100,
      int(random(60, 100))); //brightness
      weight = random(maxWeight, maxWeight+weightThreshold); //size
    }
    //colorD = color(int(random(0, 360)), int(random(0, 100)), int(random(0, 100)));
    colorR = color(int(random(0, 360)), int(random(0, 100)), int(random(0, 100)));
    //weight = random(maxWeight-weightThreshold, maxWeight+weightThreshold);
    curlyD = setCurly();
    curlyR = setCurly();
    
    //Noise code inspired by M1503
    if(curlyD) { 
      noiseScale = curlyNoiseScale; 
      //if (debug) { colorD = color(0, 100, 100); } //testing flying patterns more visibly
    }
    else { 
      noiseScale = normalNoiseScale; 
      //if (debug){ colorD = color(60, 100, 100); }
    }
    setNoiseZRange(0.4);
  }

  // create agent from 2 parents
  Agent(Agent f, Agent m) {
    //position
    x = (f.x + m.x)/2;
    y = (f.y + m.y)/2;
    px = random (x-maxStepSize, x+maxStepSize);
    py = random (y-maxStepSize, y+maxStepSize);
    
    // attributes
    age = millis();
    setSex();
    if (sex == "M") { //male
      colorD = color((hue(m.colorD) + hue(f.colorD))/2, //hue
      //int(random(0, 100)), //saturation
      100,
      brightness(m.colorD)); //brightness
      weight = m.weight; //size
    }
    else { //female
      colorD = color((hue(m.colorD) + hue(f.colorD))/2, //hue
      //int(random(0, 100)), //saturation
      100,
      brightness(f.colorD)); //brightness
      weight = f.weight; //size
    }
    colorR = color(int(random(0, 360)), int(random(0, 100)), int(random(0, 100)));
    
    if((m.curlyD || m.curlyR) && (f.curlyD || m.curlyR)) { //both parents have at least 1 curly gene
      if ((m.curlyD && m.curlyR) || (f.curlyD && m.curlyR)) { // at least 1 parent has both curly genes
        if ((m.curlyD && m.curlyR) && (f.curlyD && m.curlyR)) { // both parents carry both curly (cc x cc)
          curlyD = true;
          curlyR = true;
        }
        else { // 1 parent has both curly, other has 1 of each (cc x Cc)
          float p1 = random(1.00);
          float p2 = random(1.00);
          if (p1 > 0.5) {
            curlyD = true; // 50% curly dominates
          }
          else {
            curlyD = false;
          }
          if (p2 > 0.5) {
            curlyR = true;
          }
          else {
            curlyR = false;
          }
        }
      } 
      else { // both parents have 1 of each curly/not curly gene (Cc x Cc)
        float p1 = random(1.00);
        float p2 = random(1.00);
        //print(p1);
        if (p1 > 0.25) { curlyD = false; } //75% chance normal dominates
        else { curlyD = true; }
        if (p2 > 0.25) { curlyR = false; } //75% chance normal dominates
        else { curlyR = true; }
      }
    }
    else { //both parents carry both not curly (CC x CC)
      curlyD = false;
      curlyR = false;
    }
    
    //Noise code inspired by M1503
    if(curlyD) { 
      noiseScale = curlyNoiseScale; 
    }
    else { 
      noiseScale = normalNoiseScale;
    }
    setNoiseZRange(0.4);
  }

  void update() {
    
    px = x;
    py = y;

    if(millis() - age >= deathAge) {
      dead = true;
      y += maxStepSize/2;
    } else {
      
      // pick a new position
      //inspired by M1503
      angle = noise(x/noiseScale, y/noiseScale, noiseZ) * noiseStrength;
      angle = (angle - int(angle)) * noiseStrength;
      
      x += cos(angle) * maxStepSize;
      y += sin(angle) * maxStepSize;
  
      if (x < (width-height)/2 - 10) { x=px=width - (width-height)/2 + 10; }
      if (x > width - (width-height)/2 + 10) { x=px=(width-height)/2 - 10; }
      if (y < -10) { y=py=height + 10; }
      if (y > height + 10) { y=py= -10; }
    }
    
    //weight = maxWeight;
    noStroke();
    ellipse(x, y, weight, weight);
    
    //px = x;
    //py = y;
    noiseZ += noiseZVelocity;
  }
  
  void setNoiseZRange(float theNoiseZRange) {
    noiseZ = random(theNoiseZRange);
  }
  
  void setSex() {
    String[] sexes = { "M", "F" };
    int index = int(random(sexes.length));
    sex = sexes[index];
  }
  
  Boolean setCurly() {
    Boolean[] wings = { true, false };
    int index = int(random(wings.length));
    return wings[index];
  }

  void draw() {

    // draw a line between last position and current position
    strokeWeight(weight);
    stroke(colorD, opacity);
    line(px, py, x, y);
  }
}
