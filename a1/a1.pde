import lord_of_galaxy.timing_utils.*;
import com.hamoid.*;
import processing.sound.*;

//Code started from agentstarter

// sound
SoundFile buzz;

float radius;
PImage viewfinder;

// list of agents
ArrayList<Agent> agents;
int agentsCount;

//for frequency direction changes
int frameCount = 0;
int frameBias = 1; //slow down movement based on framerate

//noise code inspired by drawnoise & M1503
float overlayAlpha = 50;
float normalNoiseScale = 80;
float curlyNoiseScale = 750; // larger values will make "curlier" paths
float noiseStrength = 10;
float noiseZRange = 0.3; // small values will increase grouping of the agents

// agent parameters
float maxStepSize = 7;
float opacity = 100;
float maxWeight = 30;
float weightThreshold = 10;
float matureAge = 3000; //3s
float deathAge = 120000; //2min

// recording variables using the Video Export library
String videoName = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance());
VideoExport videoExport;
boolean recording = true; //true starts recording from the beginning

//gui and testing
Gui gui;
boolean showGUI = false; //only show gui for development purposes
boolean debug = false;
Stopwatch s = new Stopwatch(this);
int offspringThreshold = 1000;

void setup() {
  //size(800, 600);
  fullScreen();
  
  buzz = new SoundFile(this, "82077__benboncan__fly-2.mp3");
  buzz.loop();

  agentsCount = 50; // at least 1 of each gene variant and sex (not necessarily visible)
  radius = min(width, height)/2;
  print(radius);

  if(recording) {
    videoExport = new VideoExport(this, videoName + ".mp4");
    videoExport.startMovie();
  }
  
  // setup the simple Gui
  if(showGUI) {
    gui = new Gui(this);
    gui.addSlider("agentsCount", 10, height);
    gui.addSlider("maxStepSize", 0, 10);
    gui.addSlider("opacity", 0, 100);
    gui.addSlider("maxWeight", 0, 100);
    gui.addSlider("weightThreshold", 0, 100);
    gui.addSlider("frameBias", 1, 100);
    gui.addSlider("overlayAlpha", 0, 100);
    gui.addSlider("normalNoiseScale", 1, 1000);
    gui.addSlider("curlyNoiseScale", 1, 1000);
    gui.addSlider("noiseStrength", 1, 100);
    gui.addSlider("noiseZRange", 1, 10);
    gui.addSlider("offspringThreshold", 1, 5000);
  }
  
  colorMode(HSB, 360, 100, 100);
  
  // viewfinder
  imageMode(CENTER);
  viewfinder = loadImage("cutout.png");
  viewfinder.resize(min(width, height), min(width, height));
  
  createAgents();
}

void createAgents() {

  background(0, 0, 100); // colour
  // create Agents in a centred starting grid
  agents = new ArrayList<Agent>();
  
  for (int i = 0; i < agentsCount; i++) {
    Agent a = new Agent();
    agents.add(a);
  }
}

void draw() {
  
  // allows flies to leave a trail that fades over time
  fill(0, 0, 100, overlayAlpha);
  noStroke();
  rect(0, 0, width, height);

  // update all agents
  for (Agent a : agents) {
    a.update();
  }
  
  // collision results in offspring
  // inspired by drawlines
  ArrayList<Agent> offspring = new ArrayList<Agent>();
  if (s.isPaused() || s.time() > offspringThreshold) {
    for (int i = 0; i < agents.size(); i++) {
      for (int j = i + 1; j < agents.size(); j++) {
        
        Agent a = agents.get(i);
        Agent b = agents.get(j);
  
        float threshold = a.weight / 2 + b.weight / 2;
        float d = dist(a.x, a.y, b.x, b.y);
        if (d < threshold && //enough time since last offspring
            a.sex != b.sex && //opposite sex
            millis() - a.age >= matureAge && //"larvae" can't reproduce
            millis() - b.age >= matureAge &&
            !a.dead && !b.dead) { //can't reproduce with dead flies
          Agent o;
          if(a.sex == "F") { o = new Agent(a, b); }
          else { o = new Agent(b, a); }
          offspring.add(o);
          s.pause();
          break;
        }
      }
      if(s.isPaused()) {
        s.restart();
        break;
      }
    }
    for (int i = 0; i < offspring.size(); i++) {
      agents.add(offspring.get(i));
    }
  }
  
  //end of life
  for (int i = agents.size() - 1; i >= 0; --i) {
    Agent a = agents.get(i);
    if (a.dead && a.y >= height) {
      agents.remove(i);
    }
  }
  if(agents.size() == 0) {
    createAgents();
  }
  
  // draw all the agents
  for (Agent a : agents) {
    a.draw();
  }
  
  //draw viewfinder
  if(!debug) {
    image(viewfinder, width/2, height/2);
    noStroke();
    fill(0, 0, 0);
    rect(0, 0, (width - height)/2, height);
    rect((width - height)/2 + height, 0, (width - height)/2, height);
  }

  // draw Gui last
  if(showGUI) { gui.draw(); }
  
  // save frame for recording
  if(recording) { videoExport.saveFrame(); }
  ++frameCount;
}

// enables shortcut keys for the Gui
void keyPressed() {
  if(showGUI) { gui.keyPressed(); }

  // space to reset all agents
  if (key == ' ') {
    createAgents();
  }
  
  // allow saving the frame too
  if (key=='s' || key=='S') {
    String t = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance()); 
    String fn = t + ".png";
    println("saving frame as '" + fn + "'");
    saveFrame(fn);
  }
  
  // screen record
  if (key=='r' || key=='R') {
    recording = !recording;
    println("Recording " + (recording ? "started" : "stopped"));
    if (recording) {
      videoExport = new VideoExport(this, videoName + ".mp4");
      videoExport.startMovie();
    }
    if (!recording) { //if stopped, save and create new filename
      videoExport.endMovie();
      videoName = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance());
    }
  }
}

// call back from Gui
void agentsCount(int n) {
  agentsCount = n;
  createAgents();
}
