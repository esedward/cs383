//Code started from agentstarter

class Agent {
  //current position
  float x, y;
  //previous position
  float px, py;
  float weight;
  int speed;
  int step = 20;
  String direction;
  String type = condition; //rain/snow/dust
  
  Agent() {
    y = random(0, height);
    x = random(0, width);
    px = x;
    py = y;
    speed = 0;
    direction = windDir;
    
    if(type == "snow" || type == "dust") {
      step = 2;
    }
    
    //wind tiers determined by https://www.canada.ca/en/environment-climate-change/services/general-marine-weather-information/understanding-forecasts/beaufort-wind-scale-table.html
    if(windSpeed > 5 && windSpeed < 103) { //normal wind
      if (type == "rain") { 
        speed = (int) map(windSpeed, 6, 102, 2, 50);
      } else {
        speed = (int) map(windSpeed, 6, 102, 1, 15);
      }
    } else if(windSpeed >= 103) { //violent storm
      if (type == "rain") { 
        speed = 50;
      } else {
        speed = 20;
      }
      step = 0;
    }
  }
  
  void update() {
    px = x;
    py = y;
    
    y += step;
    
    if(direction.contains("W")) {
      x -= speed;
    } else if(direction.contains("E")) {
      x += speed;
    }
    
    if(y > height) {
      y = 0;
      x = random(0, width);
    } else if(x < 0) {
      x = width;
    } else if(x > width) {
      x = 0;
    }
  }
  
  void draw() {
    if(condition == "rain") {
      strokeWeight(2);
      stroke(color(195, 100, 84));
      if(abs(y - py) == step && abs(x - px) == speed) {
        line(px, py, x, y);
      }
    } else if(condition == "snow") {
      noStroke();
      fill(0, 0, 100);
      circle(x, y, 3);
    } else if(condition == "dust") {
      if(windSpeed > 20 && (windDir.contains("W") || windDir.contains("E"))) {
        noStroke();
        fill(0, 0, 85, 25);
        circle(x, y, 30);
      }
    }
  }
}
