//imports
import java.util.Calendar;
import lord_of_galaxy.timing_utils.*;
import com.hamoid.*;

//Started from forecast/rss code

//setup variables
XML xml;
color b1, b2, b3, b4;
ArrayList<Agent> agents;
int precipitationAgents = 75;

//location variables, default is KW
String city;
String prov = "on";
int cityCode = 82;

//tables
String[] provinces = {"ab", "bc", "mb", "nb", "nl", "nt", "ns", "nu", "on", "pe", "qc", "sk", "yt"};
String[] weekdays = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
String[] conditions = {"Sunny", "Clear", "Chance of showers"};
Calendar cal = Calendar.getInstance();
String day = weekdays[cal.get(Calendar.DAY_OF_WEEK) - 1];

//weather variables
int high = 0;
int low = 0;
int avg = 0;
String condition = "Clear";
//int pressure;
//int visibility;
//int humidity;
//int dew;
int windSpeed = 0;
String windDir = "";
//int air;

//recording
String videoName = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance());
VideoExport videoExport;
boolean recording = true; //true starts recording from the beginning

void setup() {
  //fullScreen();
  size(480, 800);
  
  if(recording) {
    videoExport = new VideoExport(this, videoName + ".mp4");
    videoExport.startMovie();
  }
  
  println(day);

  String url = "https://weather.gc.ca/rss/city/" + prov + "-" + cityCode + "_e.xml";
  //TODO add variables to change location
  println("Loading: " + url);
  println("  loaded");
  
  // try to get the RSS feed for the city, if not found return false
  try { 
    xml = loadXML(url);
    if (xml == null) {
      println("invalid city code ");
      return;
    }
  } 
  catch (Exception e) {
    println("invalid city code ");
    return;
  }
  
  //city = xml.getChild("title").getContent().split(" - ")[0].trim();
  //println("  City: " + city);
  
  getWeather(prov, cityCode);
  createAgents();
}

void draw() {
  //background(0);
  //println("high: " + high + "\navg: " + avg + "\nlow: " + low);
  setGradient(0, 0, width, height/2, b1, b2);
  setGradient(0, height/2, width, height/2, b3, b4);
  for (Agent a : agents) {
    a.update();
  }
  for (Agent a : agents) {
    a.draw();
  }
  textSize(40);
  textAlign(CENTER, CENTER);
  fill(0,0,0);
  text(city, width / 2, height / 2 - 30);
  
  // save frame for recording
  if(recording) { videoExport.saveFrame(); }
}

void createAgents() {
  agents = new ArrayList<Agent>();
  for (int i = 0; i < precipitationAgents; ++i) {
    Agent a = new Agent();
    agents.add(a);
  }
}

//taken from https://processing.org/examples/lineargradient.html
void setGradient(int x, int y, float w, float h, color c1, color c2) {
  noFill();
  for (int i = y; i <= y+h; i++) {
    float inter = map(i, y, y+h, 0, 1);
    color c = lerpColor(c1, c2, inter);
    stroke(c);
    line(x, i, x+w, i);
  }
}

float getTempHue(int temp) {
  float hue;
  if(temp > 0) {
    if (temp > 35) { 
      temp = 35; 
    }
    hue = map(temp, 0, 35, 60, 0);
  }
  else {
    if(temp < -40) {
      temp = -40;
    }
    hue = map(abs(temp), 0, 40, 160, 260);
  }
  return hue;
}

//from forecast

void keyPressed() {
  if (key == ' ') {
    boolean success = false;
    while (!success) {
      int cityCode = int(random(1, 100)); // I have no idea how many are in each province
      String prov = provinces[int(random(0, provinces.length))];
      success = getWeather(prov, cityCode);
    }
  }
  
  // allow saving the frame too
  if (key=='s' || key=='S') {
    String t = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance()); 
    String fn = t + ".png";
    println("saving frame as '" + fn + "'");
    saveFrame(fn);
  }
  
  // screen record
  if (key=='r' || key=='R') {
    recording = !recording;
    println("Recording " + (recording ? "started" : "stopped"));
    if (recording) {
      videoExport = new VideoExport(this, videoName + ".mp4");
      videoExport.startMovie();
    }
    if (!recording) { //if stopped, save and create new filename
      videoExport.endMovie();
      videoName = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance());
    }
  }
}

// local weather forecast
boolean getWeather(String prov, int cityCode) {

  String url = "https://weather.gc.ca/rss/city/" + prov + "-" + cityCode + "_e.xml";
  println(url);

  // try to get the RSS feed for the city, if not found return false
  try { 
    xml = loadXML(url);
    if (xml == null) {
      println("invalid city code " + cityCode);
      return false;
    }
  } 
  catch (Exception e) {
    println("invalid city code " + cityCode);
    return false;
  }

  // now process the XML from the feed
  //println(url, xml);

  // get city name
  city = xml.getChild("title").getContent().split(" - ")[0].trim();
  println(city);
  
  XML[] entries = xml.getChildren("entry");
  //println(entries.length);
  for (XML x : entries) {
    String entryTitle = x.getChild("title").getContent();
    //println(entryTitle);
    if (entryTitle.startsWith("Current Conditions")) {
      //println("Current Conditions");
      String summary = x.getChild("summary").getContent();
      String[] s = summary.split("\n");
      if(s != null) {
        for(String parse : s) {
          if(parse.startsWith("<b>Condition:")) {
            condition = parse;
          }
          else if(parse.startsWith("<b>Temperature:")) {
            String[] m = match(parse, "</b> (.*?)&");
            if(m != null && m.length > 1) {
              try {
                high = parseInt(m[1]);
              } catch(NumberFormatException e) {
                high = 0;
              }
              //println("high = " + high);
            }
          }
          //else if(parse.startsWith("<b>Pressure:")) {
          //  String[] m = match(parse, "</b> (.*?) kPa");
          //  try {
          //    pressure = parseInt(m[1]);
          //  } catch(NumberFormatException e) {
          //    pressure = 0;
          //  }
          //  //println(pressure);
          //}
          //else if(parse.startsWith("<b>Visibility:")) {
          //  String[] m = match(parse, "</b> (.*?) km");
          //  try {
          //    visibility = parseInt(m[1]);
          //  } catch(NumberFormatException e) {
          //    visibility = 0;
          //  }
          //  //println(visibility);
          //}
          //else if(parse.startsWith("<b>Humidity:")) {
          //  String[] m = match(parse, "</b> (.*?) %");
          //  try {
          //    humidity = parseInt(m[1]);
          //  } catch(NumberFormatException e) {
          //    humidity = 0;
          //  }
          //  //println(humidity);
          //}
          //else if(parse.startsWith("<b>Dewpoint:")) {
          //  String[] m = match(parse, "</b> (.*?)&");
          //  try {
          //    dew = parseInt(m[1]);
          //  } catch(NumberFormatException e) {
          //    dew = 0;
          //  }
          //  //println(dew);
          //}
          else if(parse.startsWith("<b>Wind:")) {
            String[] m = match(parse, "</b> (.*?) km/h");
            if(m != null && m.length > 1) {
              for(String m1 : m) { println(m1); }
              try {
                m = m[1].split(" ");
                if(m != null && m.length >1) {
                  windDir = m[0];
                  windSpeed = parseInt(m[1]);
                }
              } catch(NumberFormatException e) {
                windSpeed = 0;
              }
              //println(windSpeed + windDir);
            }
          }
          //else if(parse.startsWith("<b>Air Quality Health Index:")) {
          //  String[] m = match(parse, "</b> (.*?) <br/>");
          //  try {
          //    air = parseInt(m[1]);
          //  } catch(NumberFormatException e) {
          //    air = 0;
          //  }
          //  //println(air);
          //}
        }
      }
    }
    else if (entryTitle.startsWith(day)) {
      String[] s = entryTitle.split(" ");
      if(s != null && s.length > 1) {
        Boolean negative = s[s.length - 2].startsWith("minus");
        if (entryTitle.contains("Low")) {
          try {
            low = parseInt(s[s.length - 1]);
          } catch(NumberFormatException e) {
            low = 0;
          }
          if(negative) {
            low *= -1;
          }
        }
        else if(entryTitle.contains("High")) {
          try {
            high = parseInt(s[s.length - 1]);
          } catch(NumberFormatException e) {
            high = 0;
          }
          if(negative) {
            high = high * -1;
          }
        }
      }
    }
  }
  
  if(condition!=null) {
    if(condition.contains("POP") || 
       condition.contains("showers") || 
       condition.contains("rain") ||
       condition.contains("Rain")) {
       condition = "rain";
       if(high < 0) {
         condition = "snow";
       }
    } else if(condition.contains("snow") || condition.contains("Snow")) {
      condition = "snow";
    } else {
      condition = "dust";
    }
  }
  
  avg = (high + low)/2;
  //println(high + "\n" + low + "\n" + condition + "\n" + pressure + "\n" + visibility + "\n" + humidity + "\n" + dew + "\n" + windSpeed + "\n" + windDir + "\n" + air);

  colorMode(HSB, 360, 100, 100);
  b1 = color(getTempHue(high), 100, 100);
  if (high > 0 && low <= 0) {
    //println(true);
    if (avg > 0) {
      b2 = color(getTempHue(avg), 0, 100);
      b3 = color(160, 0, 100);
    } else {
      b2 = color(60, 0, 100);
      b3 = color(getTempHue(avg), 0, 100);
    }
  } else {
    b2 = color(getTempHue(avg), 100, 100);
    b3= b2;
  }
  b4 = color(getTempHue(low), 100, 100);
  
  if(condition == "dust") {
    precipitationAgents = 250;
  }
  
  //windSpeed = 50;
  //windDir = "E";
  println(windSpeed);
  println(windDir);
  //condition = "dust";
  //println(condition);
  
  //background(0);
  
  return true;
}
