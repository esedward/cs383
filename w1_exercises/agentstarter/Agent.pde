
class Agent {

  float shade;
  float weight;
  
  // current position
  float x, y;
  // previous position
  float px, py;


  // create agent that picks starting position itself
  Agent() {
    // default is all agent are at centre
    //x = width / 2;
    //y = height / 2;
    
    // random starting position
    int m = 100; // margin
    x = random(m, width - m);
    y = random(m, height - m);
    
    // pick a random grey shade
    shade = 255 * int(random(0, 2));
    
    // pick random stroke weight
    weight = random(1, maxWeight);
   
  }

  // create agent at specific starting position
  Agent(float _x, float _y) {
    x = _x;
    y = _y;
  }

  void update() {
    // save last position
    px = x;
    py = y;

    // pick a new position
    
    x = x + random(-maxStepSize, maxStepSize);
    y = y + random(-maxStepSize, maxStepSize);

  }

  void draw() {

    // draw a line between last position
    // and current position
    strokeWeight(weight);
    stroke(shade, opacity);
    line(px, py, x, y);
  }
}
