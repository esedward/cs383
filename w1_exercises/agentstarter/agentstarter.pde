/*
 * Starter code for creating a generative drawing using agents
 * 
 *
 */

Gui gui;

// list of agents
ArrayList<Agent> agents;

int agentsCount;

// add your agent parameters here
float maxStepSize = 1;
float opacity = 20;
float maxWeight;

void setup() {
  size(800, 600);
  //fullScreen();

  agentsCount = height / 3;

  // setup the simple Gui
  gui = new Gui(this);

  gui.addSlider("agentsCount", 10, height);
  gui.addSlider("maxStepSize", 0, 5);
  gui.addSlider("opacity", 0, 255);
  gui.addSlider("maxWeight", 0, 100);
  createAgents();
}

void createAgents() {

  background(255);
  // create Agents in a centred starting grid
  agents = new ArrayList<Agent>();
  
  for (int i = 0; i < agentsCount; i++) {
    Agent a = new Agent();
    agents.add(a);
  }
  
  // create Agents in a grid (not centered)
  /*for (float x = 100; x < width - 100; x += 5)
    for (float y = 100; y < height - 100; y += 5) {
      Agent a = new Agent(x, y);
      agents.add(a);
    }*/
}

void draw() {

  // update all agents
  // draw all the agents
  for (Agent a : agents) {
    a.update();
  }

  // draw all the agents
  for (Agent a : agents) {
    a.draw();
  }

  // draw Gui last
  gui.draw();

  // interactively adjust agent parameters
  //maxStepSize = map(mouseX, 0, width, 0, 10);
}

// enables shortcut keys for the Gui
// 'm' or 'M' to show menu, 'm' to partially hide, 'M' to fully hide
void keyPressed() {
  gui.keyPressed();

  // space to reset all agents
  if (key == ' ') {
    createAgents();
  }
}

// spawn new agents as you draw a line
void mouseDragged() {
  Agent a = new Agent(mouseX, mouseY);
  agents.add(a);
}

// call back from Gui
void agentsCount(int n) {
  agentsCount = n;
  createAgents();
}
