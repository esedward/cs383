// This sketch shows how to use the Amplitude class to analyze a
// stream of sound. In this case a sample is analyzed. Smooth_factor
// determines how much the signal will be smoothed on a scale
// form 0-1.

import processing.sound.*;
import com.hamoid.*;

// Declare the processing sound variables 
SoundFile sample;
Amplitude rms;

// Declare a scaling factor
float Scale=5;

// Declare a smooth factor
float smooth_factor=0.25;

// Used for smoothing
float sum, rmsPrev;

// recording variables using the Video Export library
String videoName = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance());
VideoExport videoExport;
boolean recording = false; //true starts recording from the beginning

Gui gui;

public void setup() {
    //size(640,360);
    fullScreen();

    //Load and play a soundfile and loop it
    sample = new SoundFile(this, "beat.aiff");
    sample.loop();
    sample.rate(0.6);
    
    // Create and patch the rms tracker
    rms = new Amplitude(this);
    rms.input(sample);
    
    if(recording) {
      videoExport = new VideoExport(this, videoName + ".mp4");
      videoExport.startMovie();
    }
    
    // setup the simple Gui
    gui = new Gui(this);
    gui.addSlider("Scale", 0.2, 8);
    gui.addSlider("smooth_factor", 0.0, 1.0);
}      

public void draw() {
    // Set background color, noStroke and fill color
    background(125,125,125);
    noStroke();
    colorMode(HSB, 100);
    
   if (frameCount % 20 == 0) {
      fill(random(100),100,100);
    }
    
    // smooth the rms data by smoothing factor (0-1)
    float raw = rms.analyze();
    
    // Low pass filter
    float rms = rmsPrev * (1 - smooth_factor) + raw * smooth_factor;  

    // rms.analyze() return a value between 0 and 1. It's
    float rms_scaled=(rms*Scale*height);
    rmsPrev = rms;

    // We draw an ellispe coupled to the audio analysis
    ellipse(width/2, height/2, rms_scaled, rms_scaled);
    
  // save frame for recording
  //if(recording) { videoExport.saveFrame(); }
  //++frameCount;
}

// enables shortcut keys for the Gui
void keyPressed() {
  // allow saving the frame too
  if (key=='s' || key=='S') {
    String t = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance()); 
    String fn = t + ".png";
    println("saving frame as '" + fn + "'");
    saveFrame(fn);
  }
  
  // screen record
  if (key=='r' || key=='R') {
    recording = !recording;
    println("Recording " + (recording ? "started" : "stopped"));
    if (recording) {
      videoExport = new VideoExport(this, videoName + ".mp4");
      videoExport.startMovie();
    }
    if (!recording) { //if stopped, save and create new filename
      videoExport.endMovie();
      videoName = String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", Calendar.getInstance());
    }
  }
}
