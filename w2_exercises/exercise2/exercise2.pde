import processing.sound.*;

//Sources
//1: https://freesound.org/people/s-cheremisinov/sounds/401076/
//2: https://freesound.org/people/ZenithInfinitiveStudios/sounds/401757/
//3: https://freesound.org/people/eardeer/sounds/402013/
//4: https://freesound.org/people/uhhyou/sounds/411630/
//5: https://freesound.org/people/Natty23/sounds/411748/

SoundFile[] file;

// Number of samples 
int numsounds = 5;

// An array of octaves, 1 is normal and each octave is half or twice the original. 
float[] octave = {0.25, 0.5, 1.0, 2.0, 4.0};

// playSound array defines what will be triggered
int[] playSound = {0,0,0,0,0};

// Trigger keeps track of time. 
int trigger;

//fibonacci
int n = 1;
int Fprev2 = 0;
int Fprev = 1;
int Fn = 1;

void setup(){
  //size(640, 360);
  fullScreen();
  colorMode(HSB, 360, 100, 100);
  background(255);
  
  // Create an array of empty soundfiles
  file = new SoundFile[numsounds];
  
  // Load 5 soundfiles from a folder in a for loop. By naming the files 1., 2., 3., n.aif it is easy to iterate
  // through the folder and load all files in one line of code.
  for (int i = 0; i < numsounds; i++){
    file[i] = new SoundFile(this, (i+1) + ".wav");
  }
  
  // Create a trigger which will be the basis for our random sequencer. 
  trigger = millis(); 
}

void draw(){
  
  // If the determined trigger moment in time matches up with the computer clock events get triggered.
  if (millis() > trigger){
    // Redraw the background every time to erase old rects
    background(0);
    
    // By iterating through the playSound array we check for 1 or 0, 1 plays a sound and draws a rect,
    // for 0 nothing happens.
    
    for (int i = 0; i < numsounds; i++){      
      // Check which indexes are 1 and 0.
      if (playSound[i] == 1){
          float rate;
          fill(260 + map(i, 0, numsounds, 0, 30),100, 100);
          noStroke();
          // Choose a random index of the octave array
          int oct = int(random(0,5));
          rate = octave[oct];
          // Draw the rect in the positions we defined earlier in posx
          rect(i * width/numsounds, 50, 128, height/4 * octave[oct]);
          
          // Play the soundfile from the array with the respective rate and loop set to false
          file[i].play(rate, 1.0);
      }
      
      // Renew the indexes of playSound so that at the next event the order is different and randomized.
      //playSound[i] = random(1) < 0.35 ? 1 : 0;
      playSound[i] = Fn % (i+1) == 0 ? 1 : 0;
      ++n;
      Fprev2 = Fprev;
      Fprev = Fn;
      Fn = Fprev + Fprev2;
    }
    
    // creates an offset between .2 and 1 second
    trigger = millis() + int(random(200,2000 * map(mouseX, 0, width, 0, 1)));
  }
}
