/* 
 * Face tracking
 * 
 *    Code based on demos in OpenCV for Processing 0.5.4 
 *    by Greg Borenstein http://gregborenstein.com
 */

import processing.video.*;

import gab.opencv.*;
import org.opencv.imgproc.Imgproc;

// to get Java Rectangle type
import java.awt.*; 

import lord_of_galaxy.timing_utils.*;
import com.hamoid.*;

Capture cam;
OpenCV opencv;

// scale factor to downsample frame for processing 
float scale = 0.5;

// image to display
//PImage output;
PImage drawing;
PImage emoji;

// array of bounding boxes for face
//Rectangle[] faces;

// dominant direction of image calculated by optical flow
PVector direction;

// the interactive drawing visualization
PGraphics viz;
// last point of the drawing
PVector lastPoint;

boolean makeDrawing = true;

// recording variables using the Video Export library
String videoName = "spellingmyname";
VideoExport videoExport;
boolean recording = true; //true starts recording from the beginning

void setup() {
  size(640, 480);
  ///fullScreen();
  
  if(recording) {
    videoExport = new VideoExport(this, videoName + ".mp4");
    videoExport.startMovie();
  }

  // want video frame and opencv proccessing to same size
  cam = new Capture(this, int(640 * scale), int(480 * scale), "pipeline:autovideosrc");

  opencv = new OpenCV(this, cam.width, cam.height);
  //opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE); 
  //opencv.loadCascade(OpenCV.CASCADE_MOUTH);  

  cam.start();

  // init to empty image for face
  //output = new PImage(cam.width, cam.height);
  
  // init to empty image for optical flow
  drawing = new PImage(cam.width, cam.height);

  //lastPoint = new PVector(width/2, height/2);
  lastPoint = new PVector(0, height/2);
  direction = new PVector(0, 0);

  viz = createGraphics(width, height);
  
  //emoji = loadImage("/data/mouth.png");
}


void draw() {

  if (cam.available() == true) {
    cam.read();

    // load frame into OpenCV 
    opencv.loadImage(cam);
    opencv.flip(1);
    //faces = opencv.detect();
    
    // switch to RGB mode before we grab the image to display
    //opencv.useColor(RGB);
    //output = opencv.getSnapshot(); 
    
    //opencv.loadImage(cam);
    //opencv.flip(1);
    
    // this calculates the "direction" each cell of an
    // image after it's divided into a grid
    opencv.calculateOpticalFlow();

    // calculate average direction
    direction = opencv.getAverageFlow();

    // if motion is very small, optical flow might return NaN
    // if this happens, just create a 0 vector
    if (Float.isNaN(direction.x) || Float.isNaN(direction.y)) {
      direction = new PVector();
    }

    // grab image of video frame for display
    //opencv.useColor(RGB);
    drawing = opencv.getSnapshot();
  }

  // draw the image
  pushMatrix();
  // scale the image back up for display 
  // (this is the inverse of the downsample scale)
  scale(1 / scale); 
  tint(255, 128); // partially transparent image
  image(drawing, 0, 0 );
  noTint();
  //// draw optical flow vector field for debug
  //stroke(255, 128);
  //strokeWeight(1);
  //opencv.drawOpticalFlow();
  popMatrix();

  // draw face tracking debug
  //int maxIndex = 0;
  //float s = 1 / scale;
  //if (faces != null) {
  //  for (int i = 0; i < faces.length; i++) {

  //    // scale the tracked faces to canvas size
  //    int x = int(faces[i].x * s);
  //    int y = int(faces[i].y * s);
  //    int w = int(faces[i].width * s);
  //    int h = int(faces[i].height * s);
  //    int maxW = int(faces[maxIndex].width * s);
  //    int maxH = int(faces[maxIndex].height * s);

  //    // draw bounding box and a "face id"
  //    //stroke(255, 255, 0);
  //    //noFill();     
  //    //rect(x, y, w, h);
  //    //fill(255, 255, 0);
  //    //text(i, x, y - 20);
  //    if(y > height/2 && w*h > maxW * maxH) { //only recognize the largest detected "mouth" in the lower half of the screen
  //      maxIndex = i;
  //    }
  //  }
  //}
  //image(emoji, int(faces[maxIndex].x * s), int(faces[maxIndex].y * s), int(faces[maxIndex].width * s), int(faces[maxIndex].height * s));
  //fill(255, 0, 0);
  //// nfc is a function to format numbers, second argument is 
  //// number of decimal places to show
  //text(nfc(frameRate, 1), 20, 20);
  
  //// draw the image
  //pushMatrix();
  //// scale the image back up for display 
  //// (this is the inverse of the downsample scale)
  //scale(1 / scale); 
  //tint(255, 128); // partially transparent image
  //image(drawing, 0, 0 );
  //noTint();
  
  //popMatrix();
  
  // draw direction vector
  stroke(255, 255, 0, 128);
  strokeWeight(4);
  PVector a = new PVector(width/2, height/2);
  PVector b = PVector.add(a, PVector.mult(direction, 50));
  //line(a.x, a.y, b.x, b.y);
  
  fill(255, 0, 0);
  // nfc is a function to format numbers, second argument is 
  // number of decimal places to show
  text(nfc(frameRate, 1), 20, 20);
  
  if (makeDrawing) {
    
    // this shows how to draw into an image each frame, 
    // just like drawing into a canvas background
  
    // make the drawing
    //direction = new PVector(1,1);
    int velocity = 20;
    PVector nextPoint = lastPoint;
    PVector difference = PVector.mult(direction, velocity);
    if (difference.x + lastPoint.x > lastPoint.x - velocity) {
      nextPoint = PVector.add(lastPoint, difference);
    }

    // drawing in the PGraphics
    viz.beginDraw();
    viz.stroke(255, 0, 0);
    viz.strokeWeight(4);
    viz.line(lastPoint.x, lastPoint.y, nextPoint.x, nextPoint.y);
    viz.endDraw();

    // paste the viz into the canvas
    image(viz, 0, 0);

    lastPoint = nextPoint;
  }
  // save frame for recording
  if(recording) { videoExport.saveFrame(); }
}

void mousePressed() {
  // click to make a new start location
  lastPoint = new PVector(mouseX, mouseY);
}

// enables shortcut keys for the Gui
void keyPressed() {
  // screen record
  if (key=='r' || key=='R') {
    recording = !recording;
    println("Recording " + (recording ? "started" : "stopped"));
    if (recording) {
      videoExport = new VideoExport(this, videoName + ".mp4");
      videoExport.startMovie();
    }
    if (!recording) { //if stopped, save and create new filename
      videoExport.endMovie();
      videoName = "spellingmyname";
    }
  }
}
