/*
 * csv
 *   Minimal sketch to load a csv into a `Table`,
 *   display some meta information, and iterate through the data.
 * 
 *   Based off of loadTable reference page: 
 *   https://processing.org/reference/loadTable_.html
 *
 */

Table table;
int index = 0;

void setup() {
  size(720, 240);

  // load in the csv data
  // local file (in data/" directory of this sketch)
  String fn = "ca_postal_codes.csv";
  // remember, always good to print out some information about the 
  // data you loaded to help diagnose problems
  println("Loading `" + fn + "'...");
  table = loadTable(fn, "header");
  println("  loaded " + table.getRowCount() + " rows and " +
    table.getColumnCount() + " columns");

  // print out the column names and types (good for diagnosing problems)
  for (String c : table.getColumnTitles()) {
    println(c, table.getColumnType(c));
  }

}

void draw() {
  background(255);
  //fill(0, alpha);
  //rect(0, 0, width, height);
  //fill(255);
  
  colorMode(HSB, 360, 100, 100, 100);
  
  float x = 0;
  float y = 0;
  float x_old = 0;
  float y_old = 0;

  // try sorting the data
  table.sortReverse("Longitude");

  // iterate through the rows
  for (int i = 0; i <= index && index < table.getRowCount(); i++) {
    TableRow row = table.getRow(i);
    x_old = x;
    y_old = y;
    x = map(row.getFloat("Longitude"), -139.4351, -52.6961, 0, 720);
    y = map(row.getFloat("Latitude"), 42.0377, 70.4643, 0, 240);
    
    float h = 0;
    if (y < height/2) {
      h = 230;
    }
    float b = map(y/2, 0, height/2, 0, 100);
    
    stroke(h, 100, 0);
    strokeWeight(4);
    noFill();
    if(x_old > 0 && x_old < width) {
      line(x_old, y_old, x, y);
    }
    
    fill(h, 100, b);
    noStroke();
    circle(x, y, 20);
  }
  
  // interval of frames to update the string
  int update = 16;
  
  // simple timer method to do something every few frames
  if (frameCount % update == 0) {
    index++;
  }
  index++;
}
