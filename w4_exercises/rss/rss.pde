/*
 * rss feed data demo
 *   Loads and displays a news feed from CBC
 * 
 */


void setup() {

  //String url = "http://www.cbc.ca/cmlink/rss-sports-curling";
  //String url = "http://www.ndbc.noaa.gov/data/latest_obs/51202.rss";
  //String start = "xmlns:atom=\"http://www.w3.org/2005/Atom\">";
  String url = "51202.rss";
  XML xml = loadXML(url);
  
  println("Loading rss feed: " + url);
  println("  loaded");
  //String description = xml.getChild("channel").getChild("description").getContent();
  //println(description);
  
  String[] m = match(xml.toString(), "Significant Wave Height.*ft");
  m = m[0].split("</strong>");  
  println(m[0] + m[1]);
}
