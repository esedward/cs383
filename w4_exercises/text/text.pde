/*
 * text data demo
 *   Loads text and prints it to the console. 
 */

int index = 0;
String all;

void setup() {
  size(720, 480);
 
  // you can also load from the web by giving a URL
  //String fn = "http://www.gutenberg.org/cache/epub/2489/pg2489.txt";
  String fn = "https://raw.githubusercontent.com/processing/processing/master/core/src/processing/core/PApplet.java";
  String start = " * than working around legacy Java code.";
  
  // this file better be in the /data subdirectory
  //String fn = "1342-0.txt";
  
  // useful to print some messages to the console to track down bugs and 
  // problems with data sourses
  println("Loading `" + fn + "'...");
  String[] lines = loadStrings(fn);
  println("  loaded " + lines.length + " lines");  
  
  // loadStrings returns an array containing each line of the text file
  // if you want the whole file as one big string (with new line chars too),
  // join the lines back into one big string (with new lines inserted)
  //all = String.join("\n", lines);
    
  // for now, we just print everything to the console
  //println(all);
  
  // strip out guttenberg frontmatter and endmatter
  StringBuilder s = new StringBuilder();
  boolean frontmatter = true;
  for (String l: lines) {
    if (frontmatter && l.contains(start)) {
      frontmatter = false;
    } else if (!frontmatter) {
      s.append(l + " ");
    }
  }
  all = s.toString();
  println("  found " + all.length() + " characters in book");

  // setup text style
  textSize(60);
  textAlign(CENTER, CENTER);
  background(0);
}

void draw() {
  //background(0);
  float alpha = adjustY(0, 50); // 10 looks nice
  fill(0, alpha);
  rect(0, 0, width, height);
   
  fill(255);
  float x = width/2;
  float y = height/2;
  //// float the text up and down
  //float floatRange = 100;
  //y += -floatRange/2 + floatRange * noise(frameCount/500.0);
  
  if (index < all.length()) {
    if (all.charAt(index) == '(' ||
        all.charAt(index) == ')' ||
        all.charAt(index) == '[' ||
        all.charAt(index) == ']' ||
        all.charAt(index) == '{' ||
        all.charAt(index) == '}') {
          text(all.charAt(index), x, y);
        }
    else { index++; }
  }
  else { println("no more brackets"); }
  
  // interval of frames to update the string
  int update = int(adjustX(1, 60)); // ~16 looks nice
  
  // simple timer method to do something every few frames
  if (frameCount % update == 0) {
    index++;
  }
}

// helper functions to adjust values with mouse

float adjustX(float low, float high) {
   float v = map(mouseX, 0, width - 1, low, high); 
   //println("adjustX: ", v);
   return v;
}

float adjustY(float low, float high) {
   float v = map(mouseY, 0, height - 1, low, high); 
   //println("adjustY: ", v);
   return v;
}
