import websockets.*;
import java.util.concurrent.ConcurrentLinkedQueue;

WebsocketClient wsc;

int r;
int g;
int b;
color c = color(r, g, b);

void setup() {
  size(400, 400);

  // make sure server is already running on this port
  wsc = new WebsocketClient(this, "ws://localhost:3001");
  //wsc = new WebsocketClient(this, "ws://192.168.1.111:3001");  
}

void draw() {
  // nothing here
}


void mouseDragged() {
  // first create message to send to server
  wsc.sendMessage(mouseX + "," + mouseY);
  // then draw some local feedback
  noStroke();
  fill(c);
  ellipse(mouseX, mouseY, 10, 10);
}

// data structure to hold and parse each incoming socket message
class Message {

  Message(String msg) {
    String[] t = msg.split(",");
    r = int(t[0]);
    g = int(t[1]);
    b = int(t[2]);
  }

  String toString() {
    return r + "," + g + "," + b;
  }

  int r;
  int g;
  int b;
}

void keyPressed() {
}

ConcurrentLinkedQueue<Message> q = new ConcurrentLinkedQueue<Message>();

// event callback when server sends message
// NOTE: this isn't on the drawing thread
void webSocketEvent(String msg) {
  Message m = new Message(msg);
  r = m.r;
  g = m.g;
  b = m.g;
  c = color(r, g, b);
  println(m);
  q.offer(m);
  
  // drawing here may not work because of thread boundaries and
  // different graphics contexts
  
}
