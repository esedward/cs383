import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Map;
import java.util.Set;


// data structure to store the input state of each client
class InputState {

  void update(int f, int x, int y, int c, String s, boolean click, boolean drag) {
    if (f != frameNum) {
      pmouseX = mouseX;
      pmouseY = mouseY;
    }
    mouseX = x;
    mouseY = y;
    colour = c;
    shape = s;
    mouseClicked = click;
    mouseDragged = drag;
    // to check for dead clients
    frameNum = f;
    
    if(colour <= 360) { globalColor = colour; }
    if(!(shape.equals("n"))) { globalShape = shape; }
    
  }

  // nice output for debugging
  String toString() {
    return frameNum + ": " + mouseDragged + "/" + mouseClicked +
      ",(" + pmouseX + "," + pmouseY + ")," +
      "(" + mouseX + "," + mouseY + ")," + colour + "," + shape;
  }

  // the client's state, made to look like Processing input state
  int frameNum;
  int mouseX;
  int mouseY;
  int pmouseX;
  int pmouseY;
  int colour;
  String shape;
  boolean mouseClicked;
  boolean mouseDragged;
}

// data structure to hold all clients and latest input state
HashMap<String, InputState> clients = new HashMap<String, InputState>();

// intermediate datastructure to transfer input messages 
// from socket thread to main thread
ConcurrentLinkedQueue<Message> q = new ConcurrentLinkedQueue<Message>();

// data structure to hold and parse each incoming socket message
class Message {

  Message(String msg) {
    String[] t = msg.split(",");
    id = t[0];
    e = t[1];
    x = int(t[2]);
    y = int(t[3]);
    c = int(t[4]);
    s = t[5];
  }

  String toString() {
    return id + "," + e + "," + x + "," + y + "," + c + "," + s;
  }

  String id;
  String e;
  int x;
  int y;
  int c;
  String s;
}


void updateClientStates() {

  // update state of all clients
  while (!q.isEmpty()) {
    Message m = q.poll();

    // clients is a hashmap of client id to client state
    if (!clients.containsKey(m.id)) {
      println("New Client: " + m.id);
      clients.put(m.id, new InputState());
    }

    // now we update the client state
    //if (m.e.equals("mm") || m.e.equals("md") || m.e.equals("kp") || m.e.equals("mr")) {
      clients.get(m.id).update(frameCount, m.x, m.y, m.c, m.s, m.e.equals("mr"), m.e.equals("md"));
      //if(m.e.equals("md")) {
      //  dragged = true;
      //}
    //}
    
    //if(m.e.equals("kp")) {
      //if(m.c != globalColor && m.c > 360) {
      //  globalColor = m.c;
      //}
      //else if(!(m.s.equals("n"))) {
      //  globalShape = m.s;
      //}
    //}
  }
}


// callback when client sends message
// NOTE: this isn't on the drawing thread, so you can't draw here
void webSocketServerEvent(String msg) {
  Message m = new Message(msg);
  //println(m);
  
  // add this message to the threadsafe queue
  q.offer(m);
}
